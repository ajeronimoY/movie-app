package com.example.moviewapp.ui.movie

import android.annotation.SuppressLint
import android.content.Context
import android.content.res.Configuration
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.*
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.TranslateAnimation
import androidx.appcompat.widget.PopupMenu
import androidx.core.widget.doOnTextChanged
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import com.bumptech.glide.Glide
import com.example.moviewapp.R
import com.example.moviewapp.databinding.FragmentMovieBinding
import com.example.moviewapp.databinding.MovieCardBinding
import com.example.moviewapp.domain.model.EntityMovie
import com.example.moviewapp.util.adapter.getRecyclerview
import com.example.moviewapp.util.adapter.initAdapter
import com.google.android.material.snackbar.BaseTransientBottomBar.ANIMATION_MODE_SLIDE
import com.google.android.material.snackbar.Snackbar
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.layout_snackbar_custom.view.*
import javax.inject.Inject
import kotlin.math.roundToInt
import kotlin.math.roundToLong
import kotlin.math.sqrt


class MovieFragment : DaggerFragment() {

    @Inject
    lateinit var viewmodelFactory: ViewModelProvider.Factory

    private val viewModel: MovieViewModel by lazy {
        ViewModelProvider(this,viewmodelFactory).get(MovieViewModel::class.java)
    }


    private var _binding: FragmentMovieBinding? = null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View{
        // Inflate the layout for this fragment
        _binding =  FragmentMovieBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val resultadoDevice = isDeviceTablet(requireContext())
        val columnas = if(resultadoDevice) 4 else 2
        binding.recyclerView.itemAnimator = DefaultItemAnimator()
        binding.recyclerView.layoutManager = GridLayoutManager(requireContext(),columnas)
        binding.recyclerView.initAdapter<EntityMovie>(R.layout.movie_card).onBindView { itemView, item, position ->

            val itemBinding = MovieCardBinding.bind(itemView)

            if(item?.posterPath!="null"){
                val imageUrl = "https://image.tmdb.org/t/p/w300/${item?.posterPath}"
                context?.let { Glide.with(it).load(imageUrl).into(itemBinding.idMovieImage) }
            }

            itemBinding.idMovieTitle.text = item?.title
            if (item != null) {
                itemBinding.idMovieRating.rating = item.voteAverage
            }
            itemBinding.idCheckBoxFavorite.isChecked = item?.favorite == true
            itemBinding.idCheckBoxFavorite.setOnClickListener {
                item?.favorite =itemBinding.idCheckBoxFavorite.isChecked
                if (item != null) {
                    viewModel.saveUser(item)
                }
                binding.recyclerView.getRecyclerview<EntityMovie>().updateItem(item, position)
            }

            itemBinding.imageButton.setOnClickListener {
                showPopupMenu(it, position, item?.id!!)
            }

            val text: String = itemBinding.idMovieTitle.text.toString() //get text
            val width = itemBinding.idMovieTitle.paint.measureText(text).roundToInt() //measure the text size
            val params: ViewGroup.LayoutParams = itemBinding.idMovieTitle.layoutParams
            params.width = width
            itemBinding.idMovieTitle.layoutParams = params //refine

            val realWidth = width / 1.6
            val timeAnimation = (width / 195).toDouble().roundToLong()

            if(realWidth > 190){
                val slide = TranslateAnimation(300.0f, - width.toFloat(), 0.0F,0.0F)
                slide.duration = 7000 * timeAnimation
                slide.repeatCount= Animation.INFINITE
                slide.repeatMode = Animation.RESTART
                slide.fillAfter = true
                slide.interpolator = LinearInterpolator()
                itemBinding.idMovieTitle.startAnimation(slide)
            }

        }.loadMore {
            viewModel.callApiMore(it+1)
        }



        with(viewModel){
            callApi()
            resultado.observe(viewLifecycleOwner) {
                binding.recyclerView.getRecyclerview<EntityMovie>().setItems(it)
            }

            arrayAuxiliar.observe(viewLifecycleOwner){
                binding.recyclerView.getRecyclerview<EntityMovie>().addItems(it)
            }

        }

        binding.idMovieTextField.doOnTextChanged { text, _, _, _ ->
            if(text.toString()!=""){
                viewModel.getByKeyWord(text.toString(),1)
            }else{
                viewModel.callApi()
            }
        }

    }


    private fun isDeviceTablet(context: Context): Boolean {
        return try {
            if (isTablet(context)){
                getDevice5inch(context)
            }else{
                false
            }
        }catch (e:Exception){
            false
        }
    }

    private fun getDevice5inch(context: Context):Boolean {
        return try {
            val displayMetrics = context.resources.displayMetrics
            val yinch = displayMetrics.heightPixels / displayMetrics.ydpi
            val xinch = displayMetrics.widthPixels / displayMetrics.xdpi
            val diagonal = sqrt((xinch * xinch + yinch * yinch).toDouble())
            diagonal >= 7
        }catch (e: Exception){
            false
        }
    }

    private fun isTablet(context: Context):Boolean{
        return (context.resources.configuration.screenLayout and Configuration.SCREENLAYOUT_SIZE_MASK ) >= Configuration.SCREENLAYOUT_SIZE_LARGE
    }

    private fun showPopupMenu(view: View, position: Int, id: Long) {

        val popup = PopupMenu(view.context, view)
        val inflater: MenuInflater = popup.getMenuInflater()
        inflater.inflate(R.menu.pop_up_menu, popup.getMenu())
        popup.setOnMenuItemClickListener(PopupMenu.OnMenuItemClickListener { item ->
            when(item.itemId){
                R.id.seeLaterFragment -> {
                    viewModel.addMovieToSeeLater(id).apply {
                        showCustomSnackBar("Agregado a lista - ver después!!")
                    }
                }
                else -> {
                    Log.i("Opcion","Opcion no reconocida")
                }
            }

            true
        })

        popup.show()
    }

    @SuppressLint("InflateParams")
    private fun showCustomSnackBar(mensaje:String){


        val snackbar = Snackbar.make( binding.root,"", Snackbar.LENGTH_LONG)
        val customSnackView:View = layoutInflater.inflate(R.layout.layout_snackbar_custom,null)
        val layout =  snackbar.view as Snackbar.SnackbarLayout

        snackbar.view.setBackgroundColor(Color.TRANSPARENT)
        customSnackView.text_message.text = mensaje
        //snackbar.setAnchorView(binding.recyclerView)

        layout.setPadding(0,0,0,0)
        layout.addView(customSnackView)
        snackbar.setAnimationMode(ANIMATION_MODE_SLIDE)
        snackbar.show()
    }
}