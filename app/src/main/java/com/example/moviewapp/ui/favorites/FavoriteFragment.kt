package com.example.moviewapp.ui.favorites

import android.content.Context
import android.content.res.Configuration
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import com.bumptech.glide.Glide
import com.example.moviewapp.R
import com.example.moviewapp.databinding.FragmentFavoriteBinding
import com.example.moviewapp.databinding.MovieCardBinding
import com.example.moviewapp.domain.model.EntityMovie
import com.example.moviewapp.util.adapter.getRecyclerview
import com.example.moviewapp.util.adapter.initAdapter
import dagger.android.support.DaggerFragment
import javax.inject.Inject
import kotlin.math.sqrt


class FavoriteFragment : DaggerFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory


    private val viewModel:FavoriteViewModel by lazy {
        ViewModelProvider(this,viewModelFactory).get(FavoriteViewModel::class.java)
    }

    private var _binding: FragmentFavoriteBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View  {
        // Inflate the layout for this fragment
        _binding = FragmentFavoriteBinding.inflate(layoutInflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val resultadoDevice = isDeviceTablet(requireContext())
        val columnas = if(resultadoDevice) 4 else 2

        binding.recyclerView.layoutManager = GridLayoutManager(requireContext(),columnas)
        binding.recyclerView.initAdapter<EntityMovie>(R.layout.movie_card).onBindView { itemView, item, position ->

            val itemBinding = MovieCardBinding.bind(itemView)
            if(item?.posterPath!= "null"){
                val imageUrl = "https://image.tmdb.org/t/p/w300/${item?.posterPath}"
                Glide.with(requireContext()).load(imageUrl).into(itemBinding.idMovieImage)
            }
            itemBinding.idMovieTitle.text = item?.title
            itemBinding.idCheckBoxFavorite.isChecked = item?.favorite == true
            itemBinding.idMovieRating.rating = item?.voteAverage!!
            itemBinding.idCheckBoxFavorite.setOnClickListener {
                item.favorite = itemBinding.idCheckBoxFavorite.isChecked
                viewModel.updateMovie(item)
                viewModel.getAllFavorites()

            }

        }

        with(viewModel){
            getAllFavorites()

            favoritos.observe(viewLifecycleOwner) {
                binding.recyclerView.getRecyclerview<EntityMovie>().setItems(it)
            }

            itemRemoved.observe(viewLifecycleOwner){
                binding.recyclerView.getRecyclerview<EntityMovie>().removeItem(it)
            }

        }

    }

    private fun isDeviceTablet(context: Context): Boolean {
        return try {
            if (isTablet(context)){
                getDevice5inch(context)
            }else{
                false
            }
        }catch (e:Exception){

            Log.i("FALLLOOOOOO>>>>>","TRUE")
            false
        }
    }

    private fun getDevice5inch(context: Context):Boolean {
        return try {
            val displayMetrics = context.resources.displayMetrics
            val yinch = displayMetrics.heightPixels / displayMetrics.ydpi
            val xinch = displayMetrics.widthPixels / displayMetrics.xdpi
            val diagonal = sqrt((xinch * xinch + yinch * yinch).toDouble())

            Log.i("FALLLOOOOOO>>>>>","TRUE $diagonal")
            diagonal >= 7
        }catch (e: Exception){
            Log.i("FALLLOOOOOO>>>>>","TRUE")
            false
        }
    }

    private fun isTablet(context: Context):Boolean{
        return (context.resources.configuration.screenLayout and Configuration.SCREENLAYOUT_SIZE_MASK ) >= Configuration.SCREENLAYOUT_SIZE_LARGE
    }
}