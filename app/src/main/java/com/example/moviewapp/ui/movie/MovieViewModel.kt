package com.example.moviewapp.ui.movie

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.moviewapp.domain.model.EntityDataSend
import com.example.moviewapp.domain.model.EntityMovie
import com.example.moviewapp.domain.usercase.*
import com.google.android.gms.common.util.ArrayUtils
import javax.inject.Inject

class MovieViewModel @Inject constructor(
    private val useCase: GetAllMoviesUseCase,
    private val useCaseMore: GetMoreMoviesUseCase,
    private val useCaseInfo: GetInfoMovieUseClass,
    private val useCaseFilter: GetMovieByKeyWordUseCase,
    private val useCaseUpdate: SaveMoiveUseCase
):ViewModel() {

    // by lazy inicia la instancia en la primera llamada de la variable
    val resultado:MutableLiveData<ArrayList<EntityMovie>> by lazy {
        MutableLiveData<ArrayList<EntityMovie>>()
    }
    val arrayAuxiliar: MutableLiveData<ArrayList<EntityMovie>> by lazy{
        MutableLiveData<ArrayList<EntityMovie>>()
    }

    override fun onCleared() {
        super.onCleared()
        useCase.unsubscribe()
        useCase.unsubscribe()
    }

    fun callApi(){
        useCase.executeUseCase(){
            onComplete {
                resultado.value = it
            }
            onError {
                Log.i("Data",it.toString())

            }
            onCancel {
                Log.i("Data",it.toString())
            }
        }
    }

    fun callApiMore(page:Int){
        useCaseMore.executeUseCase(page){
            onComplete {
                if (it != null) {
                    arrayAuxiliar.value = it
                }
            }
            onError {
                Log.i("Data",it.toString())

            }
            onCancel {
                Log.i("Data",it.toString())
            }
        }
    }

    fun addMovieToSeeLater(id:Long){
        useCaseInfo.executeUseCase(id) {
            onComplete {
            }
        }
    }



    fun getByKeyWord(keyWord: String, page:Int){
        useCaseFilter.executeUseCase( EntityDataSend(keywork = keyWord, page = page, year = 2022)){
            onComplete {
                resultado.value = it
            }
        }
    }

    fun saveUser(data: EntityMovie){
        useCaseUpdate.executeUseCase(data){
            onComplete {

            }
            onError {
                Log.i("Salio error", "truE")
            }
        }
    }



}

private fun <E> ArrayList<E>?.find(predicate: (E) -> Unit) {

}
