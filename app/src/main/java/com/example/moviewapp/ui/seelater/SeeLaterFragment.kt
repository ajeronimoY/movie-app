package com.example.moviewapp.ui.seelater

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.example.moviewapp.R
import com.example.moviewapp.databinding.FragmentSeeLaterBinding
import com.example.moviewapp.databinding.MovieCard2Binding
import com.example.moviewapp.domain.model.EntityMovieInfo
import com.example.moviewapp.util.adapter.getRecyclerview
import com.example.moviewapp.util.adapter.initAdapter
import dagger.android.support.DaggerFragment
import javax.inject.Inject


class SeeLaterFragment : DaggerFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel: SeeLaterViewModel by lazy {
        ViewModelProvider(this,viewModelFactory).get(SeeLaterViewModel::class.java)
    }

    private var _binding: FragmentSeeLaterBinding? = null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _binding = FragmentSeeLaterBinding.inflate(inflater, container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        binding.recyclerView.layoutManager = GridLayoutManager(requireContext(),1,GridLayoutManager.HORIZONTAL, false)
        binding.recyclerView.initAdapter<EntityMovieInfo>(R.layout.movie_card2).cancelScroll().onBindView { itemView, item, position ->

            val bindingMovie = MovieCard2Binding.bind(itemView)
            val imageUrl = "https://image.tmdb.org/t/p/w300/${item?.posterPath}"

            Glide.with(requireContext()).load(imageUrl).into(bindingMovie.idCardImage)

        }.onClick { _, item, _ ->
            if(item?.posterPath!="null" && item?.backdropPath!= "null"){
                val imageUrl = "https://image.tmdb.org/t/p/w1280/${item?.posterPath}"
                val imageUrlBackDropPath = "https://image.tmdb.org/t/p/w1280/${item?.backdropPath}"
                Glide.with(requireContext()).load(imageUrlBackDropPath).into(binding.idBackImage)
                Glide.with(requireContext()).load(imageUrl).into(binding.idMovieImage)

            }

            var textGeneres = ""
            if (item != null) {
                var conteo = item.generes.size
                item.generes.forEach {
                    conteo--
                    textGeneres+= "${it.name}"
                    when{
                        conteo>0 -> textGeneres+= ", "
                    }
                }
            }
            binding.idMovieOverview.text = item?.overview
            binding.idMovieRating.rating = item?.voteAverage!!
            binding.idMovieText.text = item.title
            binding.idMovieDate.text = item.releaseDate
            binding.idMovieGeneres.text = textGeneres



        }

        with(viewModel){
            getAllMovies()
            data.observe(viewLifecycleOwner){
                binding.recyclerView.getRecyclerview<EntityMovieInfo>().setItems(it)
            }
        }

        binding.idButtonBack.setOnClickListener {
            findNavController().popBackStack()
        }
    }

}