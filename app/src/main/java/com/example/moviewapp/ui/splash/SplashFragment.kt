package com.example.moviewapp.ui.splash

import android.animation.Animator
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.example.moviewapp.R
import com.example.moviewapp.databinding.FragmentSplashBinding
import com.example.moviewapp.util.scaleFadeIn


class SplashFragment : Fragment() {

    private var _binding: FragmentSplashBinding? = null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentSplashBinding.inflate(inflater, container, false)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.logoImageView.scaleFadeIn(2000,object:Animator.AnimatorListener{
            override fun onAnimationStart(animation: Animator?) = Unit
            override fun onAnimationCancel(animation: Animator?)  = Unit
            override fun onAnimationRepeat(animation: Animator?)  = Unit
            override fun onAnimationEnd(animation: Animator?) = run {
                findNavController().navigate(SplashFragmentDirections.actionSplashFragmentToMovieFragment())
            }
        })
    }
}