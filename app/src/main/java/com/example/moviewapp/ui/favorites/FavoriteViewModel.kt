package com.example.moviewapp.ui.favorites

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.moviewapp.domain.model.EntityMovie
import com.example.moviewapp.domain.usercase.GetAllFavoritesUseCase
import com.example.moviewapp.domain.usercase.SaveMoiveUseCase
import javax.inject.Inject

class FavoriteViewModel @Inject constructor(
    private val useCaseFavorite:GetAllFavoritesUseCase,
    private val useCaseUpdate: SaveMoiveUseCase
) : ViewModel(){

    val favoritos:MutableLiveData<ArrayList<EntityMovie>> by lazy {
        MutableLiveData<ArrayList<EntityMovie>>()
    }

    val itemRemoved: MutableLiveData<Int>  by lazy{
        MutableLiveData<Int>()
    }

    override fun onCleared() {
        super.onCleared()
        useCaseFavorite.unsubscribe()
        useCaseUpdate.unsubscribe()
    }

    fun getAllFavorites(){
        useCaseFavorite.executeUseCase(){
            onComplete {
                favoritos.value = it
            }
            onError {
                Log.i("ERROR",">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
            }
        }
    }

    fun updateMovie(data: EntityMovie){
        useCaseUpdate.executeUseCase(data) {
            onComplete {

            }
            onError {
                Log.i("ERROR",">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
            }
        }
    }

    fun deleteMovie(index: Int, id: Long){
        favoritos.value?.filter { element -> element.id == id }?.forEach {
            favoritos.value!!.remove(it)
            itemRemoved.value = index
        }

    }

}