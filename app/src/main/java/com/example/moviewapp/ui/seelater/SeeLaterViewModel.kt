package com.example.moviewapp.ui.seelater

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.moviewapp.domain.model.EntityMovieInfo
import com.example.moviewapp.domain.usercase.GetAllListSeeLater
import javax.inject.Inject

class SeeLaterViewModel @Inject constructor(
    private val useCaseGetAllListSeeLater: GetAllListSeeLater
): ViewModel(){

    val data: MutableLiveData<ArrayList<EntityMovieInfo>> by lazy {
        MutableLiveData<ArrayList<EntityMovieInfo>>()
    }

    override fun onCleared() {
        super.onCleared()
        useCaseGetAllListSeeLater.unsubscribe()
    }

    fun getAllMovies() {
        useCaseGetAllListSeeLater.executeUseCase {
            onComplete {
                if (it!=null){
                    data.value = it
                    Log.i("DAAAAAAAAAAAAAAAAAAAAAAATA", data.value.toString())
                }
            }
        }
    }

}