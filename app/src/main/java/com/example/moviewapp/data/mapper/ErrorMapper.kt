package com.example.moviewapp.data.mapper

import android.util.Log
import com.example.moviewapp.domain.model.response.ErrorModel
import com.example.moviewapp.domain.model.response.ErrorStatus
import com.google.gson.Gson
import com.google.gson.JsonObject
import okhttp3.ResponseBody
import retrofit2.HttpException
import java.io.IOException
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import javax.inject.Inject

class ErrorMapper @Inject constructor() {

    fun mapToDomaiErrorException(throwable: Throwable?):ErrorModel{
        return when(throwable){
            is HttpException-> {
                Log.e("ERROR 1",throwable.toString())
                getHttpError(throwable.response()?.errorBody(),throwable.code(),
                when(throwable.code()){
                    400 -> ErrorStatus.BAD_REQUEST
                    401 -> ErrorStatus.UNAUTHORIZED
                    403 -> ErrorStatus.FORBIDDEN
                    404 -> ErrorStatus.NOT_FOUND
                    500 -> ErrorStatus.SERVER_ERROR
                    else -> ErrorStatus.NOT_DEFINED
                })
            }
            is SocketTimeoutException -> {
                Log.e("ERROR 2",throwable.toString())
                ErrorModel("TIME OUT ",0,ErrorStatus.TIMEOUT)
            }
            is IOException -> {
                Log.e("ERROR 3",throwable.toString())
                ErrorModel("CHECK CONNECTION ",0,ErrorStatus.NO_CONNECTION)
            }
            is UnknownHostException -> {
                Log.e("ERROR 4",throwable.toString())
                ErrorModel("CHECK CONNECTION",0,ErrorStatus.NO_CONNECTION)
            }
            else -> {
                Log.e("ERROR 5",throwable.toString())
                ErrorModel("UNKNOWN",0,ErrorStatus.NOT_DEFINED)
            }
        }
    }

    /**
     * attempts to parse http response body and getPending error data from it
     *
     * @param body retrofit response body
     * @return returns an instance of [ErrorModel] with parsed data or NOT_DEFINED status
     */
    private fun getHttpError(body:ResponseBody?, code:Int, errorStatus:ErrorStatus):ErrorModel{
        return try {
            val json = Gson().fromJson(body?.toString(),JsonObject::class.java)
            ErrorModel(json.toString(),code,errorStatus)
        }catch(e:Throwable){
            e.printStackTrace()
            ErrorModel(ErrorStatus.NOT_DEFINED)
        }
    }

}