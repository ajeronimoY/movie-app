package com.example.moviewapp.data.restApis

import com.example.moviewapp.core.Config
import com.example.moviewapp.domain.model.EntityMovie
import com.example.moviewapp.domain.model.EntityMovieInfo
import com.example.moviewapp.domain.model.response.ApiResponse
import kotlinx.coroutines.Deferred
import retrofit2.http.GET
import retrofit2.http.HeaderMap
import retrofit2.http.Path
import retrofit2.http.Query

interface MovieService {
    companion object{
        private const val URL_BASE :String = "/3/discover/movie"
        private const val URL_BASE_MOVIE :String = "/3/movie"
        private const val URL_BASE_MOVIE_SEARCH:String = "/3/search/movie"
    }

    @GET(URL_BASE)
    suspend fun getAllMoview(
        @HeaderMap headers: Map<String, String>,
        @Query("apikey")apikey:String = Config.API_KEY,
        @Query("language")language:String = "es-GT",
        @Query("sort_by")sortBy:String = "popularity.desc",
        @Query("include_adult")includeAdult:Boolean = false,
        @Query("include_video")includeVideo:Boolean = false,
        @Query("page")page:Int,
        @Query("year")year:Int = 2022,
        @Query("with_watch_monetization_types")with_watch_monetization_types:Boolean = false
    ):ApiResponse<List<EntityMovie>>

    @GET(URL_BASE_MOVIE+"/{id}")
    suspend fun getMoreInfo(
        @HeaderMap headers: Map<String, String>,
        @Path("id") id:Long,
        @Query("apikey")apikey:String = Config.API_KEY,
        @Query("language")language:String = "es-GT"
    ):EntityMovieInfo?

    @GET(URL_BASE_MOVIE_SEARCH)
    suspend fun getByKeyWord(
        @HeaderMap headers: Map<String, String>,
        @Query("apikey")apikey:String = Config.API_KEY,
        @Query("language")language:String = "es-GT",
        @Query("query")query:String,
        @Query("page")page:Int,
        @Query("year")year:Int = 2022,
        @Query("include_adult")includeAdult:Boolean = false,
    ): ApiResponse<List<EntityMovie>?>
}