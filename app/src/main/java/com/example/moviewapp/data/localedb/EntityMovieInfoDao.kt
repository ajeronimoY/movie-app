package com.example.moviewapp.data.localedb

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.moviewapp.domain.model.EntityMovieInfo

@Dao
interface EntityMovieInfoDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun save(subject: EntityMovieInfo): Long

    @Query("SELECT * FROM MovieInfo WHERE id= :valor ")
    suspend fun getMovieInfo(valor:Long): EntityMovieInfo?

    @Query("SELECT *  FROM movieinfo")
    suspend fun getAllMovieInfo(): List<EntityMovieInfo>?
}