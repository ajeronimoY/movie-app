package com.example.moviewapp.data.localedb

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.moviewapp.domain.model.EntityMovie
import com.example.moviewapp.domain.model.EntityMovieInfo

@Database(
    entities = [
        EntityMovie::class,
        EntityMovieInfo::class
    ], version = ManagerDb.VERSION,exportSchema = false
)
abstract  class ManagerDb : RoomDatabase(){
    companion object{
        const val DB_NAME = "database.db"
        const val VERSION = 7
    }

    abstract fun entityMovieDao(): EntityMovieDao
    abstract fun entityMovieInfoDao(): EntityMovieInfoDao
}