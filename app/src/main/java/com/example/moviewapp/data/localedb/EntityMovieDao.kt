package com.example.moviewapp.data.localedb

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.moviewapp.domain.model.EntityMovie

@Dao
interface EntityMovieDao {

    @Query("SELECT * FROM Movie")
    suspend fun findAll(): List<EntityMovie>?

    @Query("SELECT * FROM Movie WHERE favorite = :favorite")
    suspend fun findAllFavorites(favorite:Boolean = true): List<EntityMovie>?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun save(subject: EntityMovie): Long

    @Query("SELECT * FROM Movie WHERE id = :id")
    suspend fun getById(id:Long): EntityMovie


}