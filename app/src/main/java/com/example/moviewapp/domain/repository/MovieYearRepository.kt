package com.example.moviewapp.domain.repository

import android.util.Log
import com.example.moviewapp.core.Config
import com.example.moviewapp.data.localedb.EntityMovieDao
import com.example.moviewapp.data.localedb.EntityMovieInfoDao
import com.example.moviewapp.data.restApis.MovieService
import com.example.moviewapp.domain.model.EntityMovie
import com.example.moviewapp.domain.model.EntityMovieInfo
import com.example.moviewapp.util.extensions.toArrayList
import javax.inject.Inject

class MovieYearRepository @Inject constructor(
    private val api: MovieService,
    private val movieDao: EntityMovieDao,
    private val movieInfoDao: EntityMovieInfoDao,
) : RepositoryPrueba{

    override suspend fun getAllMovies(): ArrayList<EntityMovie>? {

        return movieDao.findAll().let{
            when{
                it==null || it.isEmpty() -> {
                    Log.i("DATAAAAAAA","OBTENIENDO DE LA API")
                    val headers = mapOf("Authorization" to "Bearer ${Config.ACCESS_TOKEN}")
                    api.getAllMoview(headers,page = 1).data?.toArrayList().apply {
                        for (itemMovie in this!!){
                            movieDao.save(itemMovie)
                        }
                    }
                }
                else -> {
                    Log.i("DATAAAAAAA","OBTENIENDO DE LA BASE DE DATOS")
                    it.toArrayList()
                }
            }
        }
    }

    suspend fun getMoreMovies(page:Int): ArrayList<EntityMovie>? {

        val headers = mapOf("Authorization" to "Bearer ${Config.ACCESS_TOKEN}")
        return api.getAllMoview(headers,page = page).data?.toArrayList().apply {
            Log.i("DATAAAAAAA","OBTENIENDO DE LA API ${this.toString()}")
            for (itemMovie in this!!){
                movieDao.save(itemMovie)
            }
        }
    }

    suspend fun getMovieInfo(id:Long):EntityMovieInfo? {

        return movieInfoDao.getMovieInfo(id).let{
            when{
                it == null -> {
                    val headers = mapOf("Authorization" to "Bearer ${Config.ACCESS_TOKEN}")
                    api.getMoreInfo(headers,id = id)?.apply {
                        Log.i("DATA INFO ENCONTRADA  EN API---> ",this.toString())
                        movieInfoDao.save(this)
                    }
                }
                else -> {
                    Log.i("DATA INFO ENCONTRADA  EN LOOOOOOOCAAAAAAAL---> ",it.toString())
                    it
                }
            }
        }
    }

    suspend fun getAllMoviesByKeyWord(keyWorkd: String, page:Int):ArrayList<EntityMovie>? {
        val headers = mapOf("Authorization" to "Bearer ${Config.ACCESS_TOKEN}")
         return api.getByKeyWord(query = keyWorkd, page = page,headers = headers).data?.toArrayList()
    }

    suspend fun getAllMovieSeeLater():ArrayList<EntityMovieInfo>?{
        return movieInfoDao.getAllMovieInfo()?.toArrayList()
    }

    suspend fun saveMovie(movie:EntityMovie): EntityMovie? {
        return movieDao.save(movie).let{
            movie
        }
    }

    suspend fun getAllMoviesFavorites(): ArrayList<EntityMovie>?{
        return movieDao.findAllFavorites()?.toArrayList()
    }


}