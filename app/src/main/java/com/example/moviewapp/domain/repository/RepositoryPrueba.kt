package com.example.moviewapp.domain.repository

import com.example.moviewapp.domain.model.EntityMovie

interface RepositoryPrueba {

    suspend fun getAllMovies(): ArrayList<EntityMovie>?
}