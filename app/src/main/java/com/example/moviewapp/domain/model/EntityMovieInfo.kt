package com.example.moviewapp.domain.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.example.moviewapp.domain.model.converter.ConvertDataType
import com.google.gson.Gson
import com.google.gson.annotations.SerializedName

@Entity(tableName = "MovieInfo")
class EntityMovieInfo{

    @PrimaryKey
    @SerializedName("id")
    var id: Long = 0L

    @SerializedName("title")
    var title:String = ""

    @SerializedName("overview")
    var overview: String = ""

    @SerializedName("tagline")
    var tagline:String = ""

    @SerializedName("vote_average")
    var voteAverage: Float = 0.0f

    @SerializedName("release_date")
    var releaseDate: String = ""

    @SerializedName("poster_path")
    var posterPath :String = ""

    @SerializedName("backdrop_path")
    var backdropPath:String = ""

    @SerializedName("genres")
    @TypeConverters(ConvertDataType::class)
    var generes: List<Genere> = listOf()

    override fun toString():String  {
        return Gson().toJson(this)
    }

}
