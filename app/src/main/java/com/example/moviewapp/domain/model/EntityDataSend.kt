package com.example.moviewapp.domain.model

data class EntityDataSend(
    var keywork : String,
    var year: Int,
    var page: Int
)
