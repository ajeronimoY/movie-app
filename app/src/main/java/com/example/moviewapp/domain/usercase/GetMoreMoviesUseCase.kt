package com.example.moviewapp.domain.usercase

import com.example.moviewapp.data.mapper.ErrorMapper
import com.example.moviewapp.domain.model.EntityMovie
import com.example.moviewapp.domain.repository.MovieYearRepository
import com.example.moviewapp.domain.usercase.base.BaseUserCase
import javax.inject.Inject

class GetMoreMoviesUseCase @Inject constructor(
    private val repository: MovieYearRepository,
    errorMapper: ErrorMapper
): BaseUserCase<Int, ArrayList<EntityMovie>?>(errorMapper){

    override suspend fun executeOnBackgroundCorutine(data: Int): ArrayList<EntityMovie>? {
        return repository.getMoreMovies(data)
    }
}