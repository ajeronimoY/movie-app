package com.example.moviewapp.domain.repository

import kotlinx.coroutines.Deferred

interface Repository {

    /**
     * A generic class that can provide a resource backed by both the sqlite database and the network.
     * @param <ResultType>
     * @param <RequestType>
    </RequestType></ResultType> */
    abstract  class NetworkBound<ResultType,RequestType>{
        suspend fun call():ResultType{
            val dbSource = loadFromDB()
            return if(shouldFetch(dbSource)){
                fetchFromNetwork()
            }else{
                dbSource
            }
        }

        private suspend fun fetchFromNetwork(): ResultType {
            saveCallResult(createCallAsync()?.await())
            return loadFromDB()
        }


        protected abstract suspend fun saveCallResult(item: RequestType?)
        protected abstract fun shouldFetch(data: ResultType?): Boolean
        protected abstract suspend fun loadFromDB(): ResultType
        protected abstract suspend fun createCallAsync(): Deferred<RequestType>?
    }



}