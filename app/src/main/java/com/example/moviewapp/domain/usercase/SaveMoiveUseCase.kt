package com.example.moviewapp.domain.usercase

import com.example.moviewapp.data.mapper.ErrorMapper
import com.example.moviewapp.domain.model.EntityMovie
import com.example.moviewapp.domain.repository.MovieYearRepository
import com.example.moviewapp.domain.usercase.base.BaseUserCase
import javax.inject.Inject

class SaveMoiveUseCase @Inject constructor(
    private val repository: MovieYearRepository,
    errorMapper: ErrorMapper
) :BaseUserCase<EntityMovie, EntityMovie?>(errorMapper) {

    override suspend fun executeOnBackgroundCorutine(data: EntityMovie): EntityMovie? {
        return repository.saveMovie(data)
    }
}