package com.example.moviewapp.domain.usercase

import com.example.moviewapp.data.mapper.ErrorMapper
import com.example.moviewapp.domain.model.EntityDataSend
import com.example.moviewapp.domain.model.EntityMovie
import com.example.moviewapp.domain.repository.MovieYearRepository
import com.example.moviewapp.domain.usercase.base.BaseUserCase
import javax.inject.Inject

class GetMovieByKeyWordUseCase @Inject constructor(
    private val repository: MovieYearRepository,
    errorMapper: ErrorMapper
): BaseUserCase<EntityDataSend?, ArrayList<EntityMovie>?>(errorMapper) {

    override suspend fun executeOnBackgroundCorutine(data: EntityDataSend?): ArrayList<EntityMovie>? {
        if (data != null) {
            return repository.getAllMoviesByKeyWord(data.keywork,data.page)
        }
        return arrayListOf<EntityMovie>()
    }

}