package com.example.moviewapp.domain.model.response

import com.google.gson.Gson
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class ApiResponse <T> (
    @SerializedName("page") @Expose var page:Int? = null,
    @SerializedName("results") @Expose var data:T? = null,
) {
    override fun toString(): String {
        return Gson().toJson(this)
    }
}