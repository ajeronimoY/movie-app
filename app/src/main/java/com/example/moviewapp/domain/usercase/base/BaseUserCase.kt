package com.example.moviewapp.domain.usercase.base

import com.example.moviewapp.data.mapper.ErrorMapper
import com.example.moviewapp.domain.model.response.ErrorModel
import kotlinx.coroutines.*
import java.lang.Exception
import kotlin.coroutines.CoroutineContext
import kotlin.reflect.jvm.internal.impl.types.ErrorUtils

//Función que se ejecuta en el contexto del receptor     A.(B)) -> C
// A es el receptor, B es el parametro de la función y C es el retorno de la función
typealias funcionConReceptor<T> = BaseUserCase.Request<T>.() -> Unit

abstract class BaseUserCase<in Entrada: Any?, Salida>(private val errorUtil: ErrorMapper) {

    private var parentJob: Job = Job()

    private var backgroundContext: CoroutineContext = Dispatchers.IO
    private var foregroundContext: CoroutineContext = Dispatchers.Main

    protected abstract suspend fun executeOnBackgroundCorutine(data: Entrada):Salida

    /*
    * data:Entrada = Any() as Entrada, especifica que el tipo de dato por defecto es Any casteado a Entrada
    *
    * */
    fun executeUseCase(data: Entrada = Any() as Entrada, block: funcionConReceptor<Salida> ){
        val response =Request<Salida>().apply {
            block()
        }
        unsubscribe()
        parentJob = Job()
        CoroutineScope(foregroundContext + parentJob).launch {
            try{
                response.invoke(withContext(backgroundContext){
                    executeOnBackgroundCorutine(data)
                })
            } catch (cancellationExeption: CancellationException){
                response.invoke(CancellationException())
            } catch (e: Exception){
                response.invoke(errorUtil.mapToDomaiErrorException(e))
            }
        }
    }

    fun unsubscribe(){
        parentJob.apply {
            cancelChildren()
            cancel()
        }
    }

    class Request<Tipo>{
        private var onComplete: ((Tipo) -> Unit)?=null
        private var onError: ((ErrorModel)->Unit)?=null
        private var onCancel:((CancellationException)->Unit)?=null

        fun onComplete(block: (Tipo) -> Unit ) {
            onComplete = block
        }

        fun onError(block:(ErrorModel) -> Unit){
            onError = block
        }

        fun onCancel(block:(CancellationException)->Unit ){
            onCancel = block
        }

        operator fun invoke(result: Tipo){
            onComplete?.invoke(result)
        }


        operator fun invoke(errorModel: ErrorModel){
            onError?.invoke(errorModel)
        }


        operator fun invoke(cancellationException: CancellationException){
            onCancel?.invoke(cancellationException)
        }
    }


}