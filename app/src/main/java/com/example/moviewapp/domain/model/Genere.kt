package com.example.moviewapp.domain.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import com.google.gson.Gson
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/*@Entity(
    tableName = "Genere",
    foreignKeys = arrayOf(
        ForeignKey(
            entity = EntityMovieInfo::class,
            parentColumns = ["id"],
            childColumns = ["parent_fk"]
        )
    )

)*/

class Genere {

    @SerializedName("id")
    @ColumnInfo(name = "id")
    var id: Long = 0L

    @SerializedName("name")
    @ColumnInfo(name="name")
    var name:String = ""


    /*@PrimaryKey
    @Expose
    @SerializedName("id")
    var id: Long = 0L

    @SerializedName("name")
    @Expose
    var name:String = ""


    @ColumnInfo(name = "parent_fk")
    var idParent: Long = 0L*/

    override fun toString(): String {
        return Gson().toJson(this)
    }
}