package com.example.moviewapp.domain.model.response

data class ErrorModel(
    val errorMessage:String?,
    val errorCode:Int?,
    @Transient var errorStatus: ErrorStatus
) {
    constructor(errorStatus: ErrorStatus):this(null,null,errorStatus)
}