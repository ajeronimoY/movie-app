package com.example.moviewapp.domain.model.response

enum class ErrorStatus {

    NO_CONNECTION,
    BAD_RESPONSE,
    TIMEOUT,
    EMPTY_RESPONSE,
    NOT_DEFINED,
    BAD_REQUEST,
    UNAUTHORIZED,
    FORBIDDEN,
    NOT_FOUND,
    SERVER_ERROR

}