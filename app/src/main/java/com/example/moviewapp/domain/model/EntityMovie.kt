package com.example.moviewapp.domain.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.Gson
import com.google.gson.annotations.SerializedName
import com.google.gson.annotations.Expose


@Entity(tableName = "Movie")
class EntityMovie {

    @SerializedName("id")
    @Expose
    @PrimaryKey
    var id:Long = 0L

    @SerializedName("title")
    @Expose
    var title:String = ""

    @SerializedName("poster_path")
    @Expose
    var posterPath: String = ""

    @SerializedName("vote_average")
    @Expose
    var voteAverage: Float = 0.0f

    var favorite:Boolean = false

    override fun toString(): String {
        return Gson().toJson(this)
    }

}