package com.example.moviewapp.domain.usercase

import com.example.moviewapp.data.mapper.ErrorMapper
import com.example.moviewapp.domain.model.EntityMovieInfo
import com.example.moviewapp.domain.repository.MovieYearRepository
import com.example.moviewapp.domain.usercase.base.BaseUserCase
import javax.inject.Inject

class GetAllListSeeLater @Inject constructor(
    private val repository: MovieYearRepository,
    errorMapper: ErrorMapper
): BaseUserCase<Any?, ArrayList<EntityMovieInfo>?>(errorMapper){

    override suspend fun executeOnBackgroundCorutine(data: Any?): ArrayList<EntityMovieInfo>? {
        return repository.getAllMovieSeeLater()
    }

}