package com.example.moviewapp.domain.model.converter

import androidx.room.TypeConverter
import com.example.moviewapp.domain.model.Genere
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type
import java.util.*


class ConvertDataType {
    val gson = Gson()
    @TypeConverter
    fun stringToList(data:String?): List<Genere>{
        if(data==null){
            return Collections.emptyList()
        }
        val listType: Type = object: TypeToken<List<Genere?>?>(){}.type
        return gson.fromJson(data,listType)
    }

    @TypeConverter
    fun listToString(someObjects:List<Genere?>?) : String{
        return gson.toJson(someObjects)
    }
}
