package com.example.moviewapp.core

import androidx.appcompat.app.AppCompatDelegate
import com.example.moviewapp.di.ApplicationComponent
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import com.example.moviewapp.di.DaggerApplicationComponent

class App:DaggerApplication() {

    lateinit var appcomponent:ApplicationComponent

    override fun onCreate() {
        super.onCreate()
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
    }

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        appcomponent = DaggerApplicationComponent.builder().application(this).build()
        //appcomponent.inject(this)
        return appcomponent
    }

}