@file:Suppress("unused")

package com.example.moviewapp.di.builder

import com.example.moviewapp.ui.favorites.FavoriteFragment
import com.example.moviewapp.ui.movie.MovieFragment
import com.example.moviewapp.ui.seelater.SeeLaterFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class MainActivityProviders {

    @ContributesAndroidInjector
    abstract fun provideMovieFragment(): MovieFragment


    @ContributesAndroidInjector
    abstract fun provideFavoriteFragment(): FavoriteFragment


    @ContributesAndroidInjector
    abstract fun provideSeeLaterFragment(): SeeLaterFragment
}
