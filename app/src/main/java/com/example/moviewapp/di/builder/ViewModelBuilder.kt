package com.example.moviewapp.di.builder

import androidx.lifecycle.ViewModelProvider
import com.example.moviewapp.di.builder.AppViewModelBuilder
import com.example.moviewapp.di.builder.RepositoryBuilder
import com.example.moviewapp.ui.ViewModelFactory
import dagger.Binds
import dagger.Module

@Module(includes = [(RepositoryBuilder::class), (AppViewModelBuilder::class)])
abstract class ViewModelBuilder {
    @Binds
    abstract fun bindViewModelFactory(viewModelFactory: ViewModelFactory): ViewModelProvider.Factory
}
