package com.example.moviewapp.di.module

import android.app.Application
import androidx.room.Room
import com.example.moviewapp.data.localedb.EntityMovieDao
import com.example.moviewapp.data.localedb.EntityMovieInfoDao
import com.example.moviewapp.data.localedb.ManagerDb
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DatabaseModule {
    @Provides
    @Singleton
    fun provideRoomDatabase(application: Application): ManagerDb{
        return Room.databaseBuilder(application, ManagerDb::class.java,ManagerDb.DB_NAME)
            .fallbackToDestructiveMigration()
            .build()
    }

    @Provides
    fun provideEntityMovieDao(managerDb: ManagerDb): EntityMovieDao {
        return managerDb.entityMovieDao()
    }

    @Provides
    fun provideEntityMovieInfoDao(managerDb: ManagerDb): EntityMovieInfoDao{
        return managerDb.entityMovieInfoDao()
    }
}