package com.example.moviewapp.di.builder

import com.example.moviewapp.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector  (modules = [MainActivityProviders::class])
    abstract fun bindMainActivity(): MainActivity

}
