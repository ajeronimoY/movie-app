@file:Suppress("unused")

package com.example.moviewapp.di.builder

import androidx.lifecycle.ViewModel
import com.example.moviewapp.di.qualifier.ViewModelKey
import com.example.moviewapp.ui.favorites.FavoriteViewModel
import com.example.moviewapp.ui.movie.MovieViewModel
import com.example.moviewapp.ui.seelater.SeeLaterViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class AppViewModelBuilder {

    @Binds
    @IntoMap
    @ViewModelKey(MovieViewModel::class)
    abstract fun bindCharactersViewModel(viewModel: MovieViewModel): ViewModel


    @Binds
    @IntoMap
    @ViewModelKey(FavoriteViewModel::class)
    abstract fun bindFavoriteViewModel(viewModel: FavoriteViewModel): ViewModel


    @Binds
    @IntoMap
    @ViewModelKey(SeeLaterViewModel::class)
    abstract fun bindSeeLaterViewModel(viewModel: SeeLaterViewModel): ViewModel



}
