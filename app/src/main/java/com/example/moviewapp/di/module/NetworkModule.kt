package com.example.moviewapp.di.module

import com.example.moviewapp.core.Config
import com.example.moviewapp.data.restApis.MovieService
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class NetworkModule {

    @Provides
    @Singleton
    fun providesMarvelApi():MovieService{
        var gson = GsonBuilder().create()
        return Retrofit.Builder()
            .baseUrl(Config.URL_API)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(MovieService::class.java)
    }
}