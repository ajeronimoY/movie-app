package com.example.moviewapp.di.builder

import com.example.moviewapp.domain.repository.MovieYearRepository
import com.example.moviewapp.domain.repository.Repository
import com.example.moviewapp.domain.repository.RepositoryPrueba
import dagger.Binds
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
abstract class RepositoryBuilder {

    @Binds
    abstract  fun bindMarvelRepository(repository:MovieYearRepository): RepositoryPrueba

}
