package com.example.moviewapp.di

import android.app.Application
import com.example.moviewapp.core.App
import com.example.moviewapp.di.builder.ActivityBuilder
import com.example.moviewapp.di.module.ContextModule
import com.example.moviewapp.di.module.DatabaseModule
import com.example.moviewapp.di.module.NetworkModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton


@Singleton
@Component(modules = [
    AndroidSupportInjectionModule::class,
    ContextModule::class,
    NetworkModule::class,
    DatabaseModule::class,
    ActivityBuilder::class
])
interface ApplicationComponent:  AndroidInjector<App>{

    @Component.Builder
    interface  Builder{
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): ApplicationComponent
    }

}