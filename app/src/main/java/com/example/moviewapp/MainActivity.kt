package com.example.moviewapp

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.drawerlayout.widget.DrawerLayout.SimpleDrawerListener
import androidx.navigation.NavController
import androidx.navigation.NavOptions
import androidx.navigation.findNavController
import androidx.navigation.ui.*
import com.example.moviewapp.data.localedb.ManagerDb
import com.example.moviewapp.databinding.ActivityInicioBinding
import com.example.moviewapp.util.hide
import com.example.moviewapp.util.hideKeyboard
import com.example.moviewapp.util.show
import com.google.android.material.appbar.MaterialToolbar
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_inicio.*
import kotlinx.coroutines.*
import javax.inject.Inject

class MainActivity : DaggerAppCompatActivity() {


    private lateinit var navController: NavController
    private lateinit var binding: ActivityInicioBinding
    private lateinit var appBarConfiguration: AppBarConfiguration

    @Inject
    lateinit var roomDatabase: ManagerDb

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        CoroutineScope(Dispatchers.Main + Job()).launch {
            withContext(Dispatchers.IO) {
                roomDatabase.clearAllTables()
            }
        }
        binding = ActivityInicioBinding.inflate(layoutInflater)

        with(binding){
            setContentView(binding.root)
            hideKeyboard()
            navController = findNavController(R.id.navigation_fragment).apply{
                addOnDestinationChangedListener { _, destination, _ ->
                    when(destination.id){
                        R.id.splashFragment ->{
                            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
                            toolbar.hide()
                        }
                        R.id.movieFragment -> {
                            appBarLayout.show()
                            toolbar.show()
                            bottomNavigationView.show()

                        }
                        R.id.favoriteFragment -> {
                            appBarLayout.show()
                            toolbar.show()
                            bottomNavigationView.show()
                        }

                        R.id.seeLaterFragment -> {
                            appBarLayout.show()
                            toolbar.hide()
                            bottomNavigationView.hide()
                        }
                    }
                }

            }

            appBarConfiguration = AppBarConfiguration(
                setOf(
                    R.id.splashFragment,
                    R.id.movieFragment,
                    R.id.favoriteFragment
                ),
                drawerLayout
            )

            //toolbar
            setSupportActionBar(binding.toolbar)
            setupActionBarWithNavController(navController,appBarConfiguration)


        }

        val options = NavOptions.Builder()
            .setLaunchSingleTop(true)
            .setEnterAnim(R.anim.slide_in_left)
            .setExitAnim(R.anim.slide_out_right)
            .setPopEnterAnim(R.anim.slide_in_right)
            .setPopExitAnim(R.anim.slide_out_left)
            .setPopUpTo(navController.graph.startDestination, false)
            .build()

        binding.toolbar.setOnMenuItemClickListener {
            navController.navigate(it.itemId,null,options)
            true
        }



        //Bottom Navigation
        binding.bottomNavigationView.setupWithNavController(navController)
        binding.bottomNavigationView.setOnNavigationItemSelectedListener{ item ->
            // Pop everything up to the reselected item
            val reselectedDestinationId = item.itemId
            navController.popBackStack()
            navController.navigate(reselectedDestinationId,null,options)
            true
        }

        //DrawerLayout
        binding.navView.setupWithNavController(navController)
        binding.navView.setNavigationItemSelectedListener {
            when(it.itemId) {
                R.id.seeLaterFragment -> {
                    closeNav()
                    navController.navigate(it.itemId,null, options)
                }
            }
            true
        }
        /*binding.navView.setNavigationItemSelectedListener{
            when(it.itemId){
                R.id.splashFragment -> MaterialAlertDialogBuilder(this@MainActivity).apply {
                    setTitle("HOLA")
                    setMessage("FFFFFF")
                    setPositiveButton("exelente") { dialog, _ ->
                        CoroutineScope(Dispatchers.Main + Job()).launch {
                            withContext(Dispatchers.IO) {
                                roomDatabase.clearAllTables()
                            }
                        }
                    }
                }.create().show()
            }
            true
        }*/

        binding.drawerLayout.addDrawerListener(object : SimpleDrawerListener() {
            override fun onDrawerSlide(drawerView: View, slideOffset: Float) {

                // Scale the View based on current slide offset
                val diffScaledOffset: Float = slideOffset * (1 - 0.7f)
                val offsetScale = 1 - diffScaledOffset
                binding.idContentView.scaleX = offsetScale
                binding.idContentView.scaleY = offsetScale

                // Translate the View, accounting for the scaled width
                val xOffset: Float = drawerView.width * slideOffset
                val xOffsetDiff: Float = binding.idContentView.width * diffScaledOffset / 2
                val xTranslation = xOffset - xOffsetDiff
                binding.idContentView.translationX = xTranslation
            }
        })

    }

    override fun onSupportNavigateUp(): Boolean {
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.pop_up_menu,menu)
        return true
    }



    override fun onBackPressed() = when {
        binding.drawerLayout.isDrawerOpen(GravityCompat.START) -> {
            binding.drawerLayout.closeDrawer(GravityCompat.START)
        }
        else -> {
            super.onBackPressed()
        }
    }

    fun closeNav() {
        if(binding.drawerLayout.isDrawerOpen(GravityCompat.START)) {
            binding.drawerLayout.closeDrawer(GravityCompat.START)
        }
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return item.onNavDestinationSelected(navController) || super.onOptionsItemSelected(item)
    }


}