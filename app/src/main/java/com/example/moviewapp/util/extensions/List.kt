package com.example.moviewapp.util.extensions

fun <T> List<T>.toArrayList():ArrayList<T>{
    return  ArrayList(this)
}