package com.example.moviewapp.util.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.moviewapp.R
import com.example.moviewapp.domain.model.EntityMovie


@Suppress("UNCHECKED_CAST")
fun <T> RecyclerView.initAdapter(layout: Int): RecyclerViewAdapter<T>{
    adapter = RecyclerViewAdapter<T>(context,layout)
    return adapter as RecyclerViewAdapter<T>
}

@Suppress("UNCHECKED_CAST")
fun <T> RecyclerView.getRecyclerview():RecyclerViewAdapter<T> {
    return adapter as RecyclerViewAdapter<T>
}

open class RecyclerViewAdapter<T>(private val context:Context, private val layout: Int)
    :RecyclerView.Adapter<RecyclerView.ViewHolder>(){
    private var scroll = true
    private var itemsPerPage  = 20
    internal var isLoading: Boolean = false
    private var bindView: ((view: View, item: T?, position: Int) -> Unit)? = null
    private var click: ((view: View, item: T?, position: Int) -> Unit)? = null
    private var loadMore: ((page: Int) -> Unit)? = null
    private var items: MutableList<Item<T>> = mutableListOf()

    enum class ViewType(val type: Int) {
        ITEM(1), PROGRESS_BAR(2)
    }

    inner  class Item<T>(var progress:Boolean = false, var item:T?=null)
    inner  class ViewHolder(itemView:View):RecyclerView.ViewHolder(itemView), View.OnClickListener{

        init {
            itemView.setOnClickListener(this)
        }

        fun bindView(position: Int) {
            bindView?.invoke(itemView, items[position].item, position)
        }

        override fun onClick(v: View) {
            click?.invoke(v,items[absoluteAdapterPosition].item,absoluteAdapterPosition)
        }


    }

    inner class ProgressViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if(viewType==ViewType.PROGRESS_BAR.type){
            ProgressViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_loader,parent, false))
        }else{
            ViewHolder(LayoutInflater.from(parent.context).inflate(layout, parent, false))
        }
    }
    
    @Suppress("RemoveRedundantQualifierName")
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) = (holder as RecyclerViewAdapter<*>.ViewHolder).bindView(position)

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) = recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)
            if(!scroll) return
            when{
                dy > 0  -> {
                    val gridLayoutManager =recyclerView.layoutManager as GridLayoutManager
                    val visibleItems = recyclerView.childCount
                    val totalItems = gridLayoutManager.itemCount
                    val positionFirstElementVisible = gridLayoutManager.findFirstVisibleItemPosition()
                    when{
                        (totalItems - visibleItems) <= (positionFirstElementVisible + 2) -> {
                            loadMore?.invoke((totalItems / this@RecyclerViewAdapter.itemsPerPage ))
                        }

                    }
                }
            }
        }
    })
    override fun getItemViewType(position: Int): Int = when {
        this.items[position].progress -> ViewType.PROGRESS_BAR.type
        else -> ViewType.ITEM.type
    }

    override fun getItemCount(): Int {
        return items.size
    }

    /* FUNCIONES PROPIAS DE LA CLASE */
    fun onBindView(listener: (view:View, item: T? , position:Int)-> Unit): RecyclerViewAdapter<T>{
        bindView = listener
        return this
    }
    fun loadMore(listener: (page:Int)-> Unit):RecyclerViewAdapter<T>{
        loadMore = listener
        return this
    }

    fun onClick(listener: (view: View, item: T?, position: Int) -> Unit):RecyclerViewAdapter<T>{
        click = listener
        return this
    }



    /* FUNCIONES PARA MANIPULAR LA LISTA*/
    fun setItems(items: MutableList<T>?): RecyclerViewAdapter<T>{
        this.items = items?.map { Item(false,it)}?.toMutableList() ?: mutableListOf()
        notifyDataSetChanged()
        return this
    }

    fun cancelScroll():RecyclerViewAdapter<T>{
        scroll = false
        return this
    }

    fun addItem(item: T):RecyclerViewAdapter<T>{
        this.items.add( Item( false, item))
        notifyItemInserted(this.items.size)
        return this
    }

    fun addItems(items: ArrayList<T>):RecyclerViewAdapter<T>{
        this.items.addAll( items.map{ Item(false, it)})
        notifyItemRangeInserted(this.items.size - 1, items.size)
        return this
    }

    fun updateItem(item: T?, position: Int){
        this.items[position] =Item(false, item)
        notifyItemChanged(position)
    }

    fun removeItem(int: Int):RecyclerViewAdapter<T>{
        try {
            this.items.removeAt(int)
            notifyItemRemoved(int)
        }catch (e:Exception){

        }
        return this
    }

    fun clear():RecyclerViewAdapter<T>{
        this.items.clear()
        notifyItemRangeRemoved(0,this.items.size)
        return this
    }

}

