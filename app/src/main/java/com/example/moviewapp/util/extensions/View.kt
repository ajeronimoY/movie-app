package com.example.moviewapp.util

import android.animation.Animator
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.view.View
import android.view.animation.AccelerateDecelerateInterpolator

const val DEFAULT_ANIMATION_DURATION_TIME = 250L

//scaleFadeIn
fun View.scaleFadeIn(duration: Long = DEFAULT_ANIMATION_DURATION_TIME) = scaleFadeIn(this, duration).start()

fun View.scaleFadeIn(duration: Long = DEFAULT_ANIMATION_DURATION_TIME, animListener: Animator.AnimatorListener) = scaleFadeIn(this, duration).apply {
    addListener(animListener)
}.start()

private fun scaleFadeIn(view: View, duration: Long = DEFAULT_ANIMATION_DURATION_TIME): AnimatorSet = AnimatorSet().apply {
    view.visibility = View.VISIBLE
    view.alpha = 0.0f
    this.play(ObjectAnimator.ofFloat(view, "scaleX", 5.0f, 1.0f).apply {
        interpolator = AccelerateDecelerateInterpolator()
        this.duration = duration
    }).with(ObjectAnimator.ofFloat(view, "scaleY", 5.0f, 1.0f).apply {
        interpolator = AccelerateDecelerateInterpolator()
        this.duration = duration
    }).with(ObjectAnimator.ofFloat(view, "alpha", 0.0f, 1.0f).apply {
        interpolator = AccelerateDecelerateInterpolator()
        this.duration = duration
    })
    this.startDelay = 0
}