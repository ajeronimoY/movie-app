package com.example.moviewapp.util.view

import android.view.ViewGroup
import java.lang.reflect.Method
import kotlin.reflect.full.declaredFunctions

class ExpandAndCollapseView {

    companion object {
        fun expand(v: ViewGroup, duration: Int) {
            slide(v, duration, true)
        }

        fun collapse(v: ViewGroup, duration: Int) {
            slide(v, duration, false)
        }

        fun slide(v: ViewGroup, duration: Int, expand: Boolean) {

        }
    }

}
